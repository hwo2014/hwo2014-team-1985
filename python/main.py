import json
import logging
from logging import handlers
import math
import os
import socket
import sys


logging.basicConfig(filename='/tmp/hwo.log', level=logging.DEBUG)
#logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)


# NOTES/TODOS
#
#  better telemetry data and race log output/logging
#  different race modes (training/qualifying/race)
#  provide track name as cmdline input
#  smarter pick of entry speeds based on lower r turns
#  switch lanes
#  better piece model
#  multi car races
#  keep track of other cars
#  find optimal route (for next turn)  (is it inside or outside lanes?)
#  find optimal route (for whole track)
#  track the per-turn limits (not just per turn type/radius)
#  explore posibility of turns with switches having a better racing line
#  avoid throttle based on car angle (gamma)
#  track car angle (gamma) rate, and spinoffs (can happen on straights!)
#  explore on the throttle coming out of a turn
#  track other cars (and learn from them)
#  determine car mass (F=ma, we know a in terms of throttle//force?, m = F/a),
#  use car dimentions and car yaw (gamma), somehow
#  explore starting traction control, track max a, and see if we can imporve it
#      with feathered throttle starts
#  turbo strategy
#     if found in training, use it and measure new acceleration / impulse?
#     in game, always use it on the final straight, no matter what
#     in game, on final lap, if turbo, find clear track (probably an outside
#        lane) to use turbo
# Ignore turns after finish line at last lap
#works


# TODO
class TelemetryLog(object):
    def __init__(self, logpath):
        self.logpath = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                                    logpath)
        self.logger = logging.getLogger('telemetry log')
        self.logger.setLevel(logging.DEBUG)
        # create rotating file handler and set level to debug
        self.fh = handlers.RotatingFileHandler(filename=self.logpath, mode='a',
                                               backupCount=1000)
        self.fh.setLevel(logging.DEBUG)
        self.fh.setFormatter(logging.Formatter('%(message)s'))
        self.logger.addHandler(self.fh)
        self.new_race_log()

    def log(self, msg):
        self.logger.debug(msg)

    def new_race_log(self):
        self.fh.doRollover()


# TODO
class RacePiece(object):

    def __init__(self, lengths=None, lanes=None, angle=None):
        if lanes is None:
            self.lanes = 1
        else:
            self.lanes = lanes
        if angle is None:
            self.angle = 0.0
        else:
            self.angle = None


class RaceTrack(object):

    def __init__(self, data):
        try:
            self.laps = data['race']['raceSession']
        except KeyError:
            self.laps = 1
        try:
            self.race_cars = data['race']['cars']
        except KeyError:
            self.race_cars = None
        for field in ('pieces', 'lanes', 'id', 'name', ):
            setattr(self, field, data['race']['track'][field])
        self._abs_distances()

    @property
    def json(self):
        return json.dumps({
            'id': self.id,
            'laps': self.laps,
            'race_cars': self.race_cars,
            'pieces': self.pieces,
            'lanes': self.lanes, })

    def _abs_distances(self):
        """Pre calculate distances of the track and each piece.
        Also, add helpful fields.
        """
        self.lap_distance = 0
        for i, piece in enumerate(self.pieces):
            if 'switch' not in piece:
                piece['switch'] = False
            self.pieces[i]['i'] = i
            print 'update abs positions', piece
            self.pieces[i]['lap_distance'] = self.lap_distance
            if 'length' in piece:
                self.lap_distance += piece['length']
                piece['angle'] = 0.0
                piece['is_turn'] = False
            elif 'angle' in piece and 'radius' in piece:
                piece['is_turn'] = True
                pct = abs(piece['angle']) / 360
                piece['length'] = 2 * math.pi * piece['radius'] * pct
                self.lap_distance += piece['length']

                # For arc pieces, there are different lengths, depending on
                # the lane.
                self.pieces[i]['lane_distances'] = dict()
                for lane in sorted(self.lanes, key=lambda x: x['index']):
                    if 'distanceFromCenter' not in lane:
                        actual_radius = piece['radius']
                    if piece['angle'] > 0:  # a right turn
                        actual_radius = (
                            piece['radius'] - lane['distanceFromCenter'])
                    else:
                        actual_radius = (
                            piece['radius'] + lane['distanceFromCenter'])
                    self.pieces[i]['lane_distances'][lane['index']] = dict(
                        radius=actual_radius,
                        length=(2 * math.pi * actual_radius * pct))
            else:
                logging.error('odd piece length data: {0}'.format(piece))
        logging.info('Track lap distance: {0}'.format(self.lap_distance))


class TrackPosition(object):

    def __init__(self, name):
        self.name = name
        self.lap = None
        self.angle = None
        self.piece_index = None
        self.position_data = None
        self.race_distance = 0.0
        self.lane = None
        self.v = None
        self.a = None
        self.this_r = None

    def __unicode__(self):
        #csv = '{}, {}, {}, {}, {}, {},'.format(
        #    self.lap, self.piece_index, self.piece_distance,
        #    self.race_distance, self.angle, self.v)
        #logging.warn(csv)
        return '{0} - l:{1} p:{2} pd:{3} rd:{4} a:{5} v:{6} acc:{7}'.format(
            self.name, self.lap, self.piece_index, self.piece_distance,
            self.race_distance, self.angle, self.v, self.a)

    def __str__(self):
        return unicode(self)

    def grab_data(self, data, track, color=None):
        for pos_data in data:
            try:
                name = pos_data['id']['name']
                color = pos_data['id']['color']
            except KeyError:
                logging.debug('skip weird position data: {0}'.format(
                    pos_data))
                continue
            if (color is not None and color == color) or name == self.name:
                self.update(pos_data, track)
                return pos_data

    def update(self, data, track):
        try:
            self.angle = data['angle']
        except KeyError:
            self.angle = None
        try:
            self.position_data = data['piecePosition']
        except KeyError:
            self.position_data = None
            return False

        for json_field, attr_name in (
                ('pieceIndex', 'piece_index'),
                ('inPieceDistance', 'piece_distance'),
                ('lap', 'lap'), ):
            try:
                setattr(self, attr_name, self.position_data[json_field])
            except KeyError:
                setattr(self, attr_name, None)

        if 'lane' in self.position_data:
            lanes = list()
            for lane in ('startLaneIndex', 'endLaneIndex'):
                try:
                    lane_pos = self.position_data['lane'][lane]
                except KeyError:
                    lane_pos = 0
                lanes.append(lane_pos)
            self.start_lane, self.end_lane = lanes
            self.lane = self.start_lane

        self.this_piece = track.pieces[self.piece_index]
        try:
            self.this_r = self.this_piece[
                'lane_distances'][self.lane]['radius']
        except KeyError:
            self.this_r = None


class SkwishyBot(object):

    def __init__(self, socket, name, key, track_name=None):
        self.socket = socket
        self.name = name
        self.key = key
        self.color = None
        self.pos = TrackPosition(self.name)
        self.throttle_pos = 0.0
        self.last_throttle_pos = 0.0
        self.max_throttle = 1.0
        self.game_tick = None
        self.last_piece_index = 0
        self.last_piece_distance = 0
        self.last_v = 0
        self.last_car_angle = 0
        self.max_turn_speed = 10000.0
#        self.max_deceleration = -0.155
        self.max_deceleration = -0.01
        self.max_deceleration = None
        self.min_deceleration = -0.01

        # deceleration depends on v, this is the ratio
        self.a_ratio = 1

        # learn/track max turn entry speeds
#        self.max_turn_speeds = dict()
        # 8.13 seconds: {90: 5.69148, 110: 6.48310}
        #self.max_turn_speeds = {90: 5.7125, 110: 6.54665}
#        self.max_turn_speeds = {
#            #0: 0.7790730194179787,
#            40: 3.0738144270774796,
#            60: 13.5997268126165807,
#            90: 5.7125,
#            110: 1.1768873814685321}
        # this is actually max turn entry speeds, by turn radius
        self.max_turn_speeds = dict()

        # best for default Finland test track
#        self.max_turn_speeds = {90: 5.7325, 110: 6.56665}
#        self.max_deceleration = -0.19  # best for Finland test track

        self.max_v = 0.0
        self.mode = 'train'  # start in training mode
        # used during training mode
        self.idle_speed = 0.5

        self._last_turn_entry_v = None

        if track_name is None:
            self.track_name = 'keimola'
        else:
            self.track_name = track_name

        self.telemetry_log = TelemetryLog('logs/foo.log')
        self.log = self.telemetry_log.log
        self.log('track: {0} bot: {1}'.format(self.track_name, self.name))

    def slow_ticks(self, final_v):
        """
        Find out how many game ticks it will take to slow from our current v
        to a final_v, given that we know the acceration ratio.

        This is not ideal, but should work well enough for now.
        """
        if self.a_ratio > 0:
            a_ratio = self.a_ratio * -1
        else:
            a_ratio = self.a_ratio
        current_v = self.pos.v
        if final_v > current_v:
            #final_v, current_v = current_v, final_v
            return 0
        v = current_v
        i = 0
        while v > final_v:
            a = a_ratio * v
            v = v + a
            i += 1
        return i + 1

    def msg(self, msg_type, data):
        self.send(json.dumps({'msgType': msg_type, 'data': data}))

    def send(self, msg):
        self.socket.sendall(msg + '\n')

    def join(self, password=None):
        join_data = {'botId': {'name': self.name, 'key': self.key},
                     'trackName': self.track_name,
                     'carCount': 1}
        if password is not None:
            join_data['password'] = password
        return self.msg('joinRace', join_data)

    def set_throttle(self, throttle_pos):
        self.last_throttle_pos = self.throttle_pos
        if throttle_pos > self.max_throttle:
            self.throttle_pos = self.max_throttle
        else:
            self.throttle_pos = throttle_pos
        self.msg('throttle', self.throttle_pos)

    def ping(self):
        self.msg('ping', {})

    def update_telemetry(self, data):
        # locate our car data, and update our position
        our_data = self.pos.grab_data(data, self.track)

        # Keep track of our last angle, in case of crash
        self.last_car_angle = self.pos.angle

        last_race_distance = self.pos.race_distance
        self.this_track_piece = self.track.pieces[self.pos.piece_index]

        # detect a piece change
        if self.last_piece_index != self.pos.piece_index:
            # TODO: check if we've skipped a piece
            self.last_piece_index = self.pos.piece_index
            piece_remainder = self.last_piece_remainder
            self.last_piece_distance = 0
        else:
            piece_remainder = 0

        # TODO - this doesn't take into account proper distances when there
        # is a lane change, because we're not yet sure how to calculate it.
        try:
            self.lane = our_data['piecePosition']['lane']['startLaneIndex']
        except KeyError:
            self.lane = 0
        # Keep track of the remaining distance on this piece/path so we can add
        # it to the next piece if we get that far during this game tick.
        if 'lane_distances' in self.this_track_piece:
            self.this_path_distance = \
                self.this_track_piece['lane_distances'][self.lane]['length']
        else:
            self.this_path_distance = self.this_track_piece['length']

        delta_distance = (self.pos.piece_distance - self.last_piece_distance +
                          piece_remainder)
        self.last_piece_distance = self.pos.piece_distance
        self.last_piece_remainder = (
            self.this_path_distance - self.pos.piece_distance)
        self.pos.race_distance = (
            self.pos.race_distance + delta_distance)  # + piece_remainder)

        self.pos.last_v = self.pos.v
        self.pos.v = self.pos.race_distance - last_race_distance

        # acceleration
        self.pos.a = self.pos.v - self.last_v
        self.last_v = self.pos.v

        # feedback
        # on the straight, if we see a better deceleration, record it.
        if abs(self.this_track_piece['angle']) == 0.0:
            if self.pos.a > -1 and self.pos.a < self.max_deceleration:
                print '  Increase max_deceleration ', self.max_deceleration, '->', self.pos.a
                self.max_deceleration = self.pos.a
        try:
            if (self.this_track_piece['radius'] < 100 and
                    self.pos.v > self.max_turn_speed):
                self.max_turn_speed = self.pos.v
        except KeyError:
            pass

        # DEBUG OUTPUTS
        #print ('pos.race_distance', self.pos.race_distance, 'dd', delta_distance,
        #       'remainder:', piece_remainder, 'v:', self.pos.v, 'pi:', self.pos.piece_index,
        #       'pd:', self.pos.piece_distance, 'lrd:', last_race_distance,
        #       'lpd:', self.last_piece_distance)

        # if we're moving, log our telemetry
        if self.pos.v > 0:
            #msg = 'gt:{0} {1} t:{2}'.format(
            #    self.game_tick, self.pos, self.throttle_pos)
            #self.log('debug {0}'.format(msg))
            self.log({'telemetry': dict(
                game_tick=self.game_tick,
                lap=self.pos.lap,
                piece_index=self.pos.piece_index,
                piece_distance=self.pos.piece_distance,
                race_distance=self.pos.race_distance,
                car_angle=self.pos.angle,
                v=self.pos.v,
                a=self.pos.a,
                throttle=self.throttle_pos,
                last_throttle=self.last_throttle_pos,
                piece_r=self.this_track_piece['angle'],
                this_r=self.pos.this_r,
            )})

        #return '{0} - l:{1} p:{2} pd:{3} rd:{4} a:{5} v:{6} acc:{7}'.format(
        #    self.name, self.lap, self.piece_index, self.piece_distance,
        #    self.race_distance, self.angle, self.v, self.a)

        else:
            # we probably crashed...
            print '.',
        self.max_v = max(self.max_v, self.pos.v)

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print('Joined')
        self.ping()

    def on_game_start(self, data):
        print('Race started')
        #self.ping()
        self.set_throttle(self.max_throttle)

    def on_your_car(self, data):
        logging.debug('your car data: {0}'.format(json.dumps(data)))
        try:
            self.color = data['color']
        except KeyError:
            self.color = None
        self.ping()

    def on_game_init(self, data):
        self.track = RaceTrack(data)
        #self.log(data)
        self.log('track: {0}'.format(self.track.json))
        try:
            cars = data['race']['cars']
        except KeyError:
            logging.warn('could not parse race cars {0}'.format(data))
            return False
        # get the car dimensions
        for car in cars:
            try:
                if car['id']['color'] != self.color:
                    continue
            except KeyError:
                logging.warn('could not parse car data {0}'.format(car))
                continue
            try:
                dimentions = car['dimensions']
            except KeyError:
                logging.warn('could not parse car dimentions {0}'.format(car))
                continue
            for key, att in (
                    ('guideFlagPosition', 'car_flag_pos'),
                    ('length', 'car_length'),
                    ('width', 'car_width')):
                try:
                    setattr(self, att, dimentions[key])
                except KeyError:
                    logging.warn('could not parse {0} dimention'.format(key))
        self.ping()

    def on_car_positions(self, data):
        self.update_telemetry(data)

        if self.mode == 'train':
            return self.do_train_mode(data)

        # if we don't yet know our max_deceleration, go into train mode
        if self.max_deceleration is None:
            self.mode = 'train'
            return self.do_train_mode(data)
        else:
            self.mode = 'fast_lap'

        if self.mode == 'fast_lap':
            self.do_fast_lap_mode(data)
        else:
            self.set_throttle(0.5)

    def do_train_mode(self, data):
        # Just in case we weren't already set to training mode:
        self.mode = 'train'

        # If we are training, find a flat piece first.
        # TODO: consider removing this...
        if self.this_track_piece['angle'] != 0.0:
            self.set_throttle(self.idle_speed)
            return False

        if self.game_tick < 1:
            self.set_throttle(self.max_throttle)
            return False

        if getattr(self, 'start_train_tick', None) is None:
            self.log('START TRAINING MODE')
            self.max_deceleration = None
            self.start_train_tick = self.game_tick
            self._train_decelerations = list()
            self._training = list()
            self._train_full_throttle_ticker = self.start_train_tick + 2
            self._train_cut_throttle_ticker = (
                self._train_full_throttle_ticker + 2)

        # Train mode: full throttle for two ticks, then cut the throttle
        # and average the deceleration over n ticks.
        self.log(' train throttle:{} tick:{} acc:{} {} cut at tick:{}'.format(
                 self.throttle_pos, self.game_tick, self.pos.a,
                 self._train_decelerations, self._train_cut_throttle_ticker))
        if self.game_tick < self._train_full_throttle_ticker:
            self.set_throttle(self.max_throttle)
            return True
        elif self.game_tick <= self._train_cut_throttle_ticker:
            print '  TRAIN:', self.pos.a
            if self.pos.a < 0.0:
                self._train_decelerations.append(self.pos.a)
                self._training.append(
                    (self.game_tick, self.pos.a, self.pos.v,
                     self.throttle_pos, self.pos.race_distance))
            else:
                self._train_cut_throttle_ticker += 1
            self.set_throttle(0.0)
            return True

        #for i, d in enumerate(self._training):
        #    self.a_ratio = self._training[i][1] / self._training[i][2]
        #    print i, 'New a_ratio:', self.a_ratio

        self.a_ratio = self._training[0][1] / self._training[0][2]
        print 'a_ratio:', self.a_ratio

        # If we got this far, training is done. Get a final avg deceleration.
        self.max_deceleration = float(sum(self._train_decelerations) /
                                      len(self._train_decelerations))
        print ('FINISH TRAINING MODE', self.max_deceleration,
               self._train_decelerations)
        self.mode = 'fast_lap'
        self.set_throttle(self.max_throttle)
        return True

    def get_next_turn_speed(self):
        # TODO: ignore turns beyond the finish line on the last lap
        next_r = self.r_at_next_turn()  # TODO: pick a lane
        if next_r == 0:
            return 0, 1000000

        try:
            max_turn_speed = self.max_turn_speeds[next_r]
        except KeyError:
            if len(self.max_turn_speeds) == 0:
                return next_r, self.max_turn_speed
            else:
                print 'finding next best radius for...', next_r
                max_turn_speed = self.max_turn_speed
                for radius in sorted(self.max_turn_speeds.keys()):
                    if radius < next_r:
                        max_turn_speed = (self.max_turn_speeds[radius] *
                                          (next_r / radius))
                        #self.max_turn_speeds[next_r] = max_turn_speed
                        print '...max:', max_turn_speed, radius, next_r

        return next_r, max_turn_speed

    def do_fast_lap_mode(self, data):
        """Fast Lap mode"""
        next_r, next_turn_speed = self.get_next_turn_speed()

        if self.pos.v < next_turn_speed:
            #print self.pos.v, '<', next_turn_speed, ' -- full speed ahead!'
            self.set_throttle(self.max_throttle)
            return True

        slow_ticks = self.slow_ticks(next_turn_speed)
        lookahead_distance = slow_ticks * self.pos.v
        print ('expecting next turn r: {0} max_v: {1} current_v: {2} '
               'slow_ticks: {3}').format(next_r, next_turn_speed, self.pos.v,
                                         slow_ticks)
        if self.r_at_lookahead(lookahead_distance) < 1:
            self.set_throttle(self.max_throttle)
            print 'full throttle'
            self._last_turn_entry_v = None
            return True
        else:
            self.set_throttle(0.00)
            print 'full brake'
            self._last_turn_entry_v = self.pos.v
            return False

    def do_fast_lap_mode_old(self, data):
        """Older fast lap mode (buggy)"""
        next_r, next_turn_speed = self.get_next_turn_speed()

        if self.pos.v < next_turn_speed:
            self.set_throttle(self.max_throttle)
            return True

        print 'expecting next turn r: {0} max_v: {1}'.format(
            next_r, next_turn_speed)

        delta_v = self.pos.v - next_turn_speed
        try:
            lookahead_ticks = abs(delta_v / self.max_deceleration)
        except (ZeroDivisionError, TypeError):
            lookahead_ticks = abs(delta_v / 0.001)
        lookahead_distance = lookahead_ticks * self.pos.v
        #print 'lookahead d', lookahead_distance, 'v', self.pos.v
        r = self.r_at_lookahead(lookahead_distance)
        #print 'testing r:', r, self.max_deceleration
        if r < 1:
            self.set_throttle(self.max_throttle)
        else:
            self.set_throttle(0.00)

    def r_at_lookahead(self, lookahead_distance, lane=None):
        """Return the radius of the track for a specific distance ahead of
        our current position.
        Assumes our lane is lane 0.
        """
        forward_position = self.last_piece_distance + lookahead_distance
        piece_index = self.pos.piece_index
        forward_piece = self.track.pieces[piece_index]
        if lane is None:
            lane = self.pos.lane
        radius = 0
        while forward_position > 0:
            if 'lane_distances' in forward_piece:
                this_path_distance = \
                    forward_piece['lane_distances'][lane]['length']
                radius = forward_piece['lane_distances'][lane]['radius']
            else:
                this_path_distance = forward_piece['length']
                radius = 0
            forward_position = forward_position - this_path_distance - self.pos.piece_distance
            if forward_position > 0:
                piece_index = piece_index + 1
                if piece_index >= len(self.track.pieces):
                    piece_index = 0
                forward_piece = self.track.pieces[piece_index]
        return radius

    def r_at_next_turn(self, lane=None):
        """Return the next radius of the track, up to a max_distance.
        Assumes our lane is lane 0, and doesn't take into account lane
        changes.
        """
        turn = self.next_turn()
        if lane is None:
            lane = self.pos.lane
        return turn['lane_distances'][lane]['radius']

    def next_turn(self, lane=None, lookahead_distance=0):
        """Return the next turn from our current position."""
        if lane is None:
            lane = self.pos.lane
        turn = None
        starting_piece_index = self.pos.piece_index
        piece_index = starting_piece_index + 1
        while piece_index != starting_piece_index:
            try:
                piece = self.track.pieces[piece_index]
                if piece['is_turn']:
                    turn = piece
                    break
                piece_index += 1
            except IndexError:
                piece_index = 0
        return turn

    def on_crash(self, data):
        logging.error('Someone crashed! {0}'.format(json.dumps(data)))
        # TODO: if we crashed, gather useful data to learn our limits

        # Learn something from this wreck
        #this_track_piece = self.track.pieces[self.pos.piece_index]

        self.log('last piece distance: {0}'.format(self.pos.piece_distance))
        self.log('last angle: {0}'.format(self.pos.angle))
        self.log('crash: {0}'.format(data))
        print self.pos.angle
        print self.pos.this_piece

        try:
            this_r = self.this_track_piece[
                'lane_distances'][self.pos.lane]['radius']
        except KeyError:
            this_r = 0
            logging.warn('crashed off a straight track!?')
        try:
            if self._last_turn_entry_v is None:
                max_entry_v = self.pos.last_v
            else:
                max_entry_v = self._last_turn_entry_v
            max_entry_v = self.pos.v  #  * 1.3
            self.max_turn_speeds[this_r] = min(self.max_turn_speeds[this_r],
                                               max_entry_v)
        except KeyError:
            self.max_turn_speeds[this_r] = self.pos.last_v * 0.5

        # sort out turn speeds
        last_greater_r_max = None
        for r in reversed(sorted(self.max_turn_speeds.keys())):
            if last_greater_r_max is not None:
                if last_greater_r_max < self.max_turn_speeds[r]:
                    self.max_turn_speeds[r] = last_greater_r_max
            last_greater_r_max = self.max_turn_speeds[r]

        print 'max turn speeds:', self.max_turn_speeds

        # TODO: track the max_gamma per radius, and mayber per velocity and/or
        # acceleration
        self.max_gamma = abs(self.pos.angle)
        self.ping()

    def on_spawn(self, data):
        self.do_idpb = True
        self.set_throttle(self.max_throttle)

    def on_game_end(self, data):
        self.log('Race ended')
        bests = list()
        try:
            bests = data['bestLaps']
        except KeyError:
            pass
        for result in bests:
            car = '{0} ({1})'.format(result['car']['name'],
                                     result['car']['color'])
            best_time = result['result']['millis'] / 1000.00
            self.log('{0} {1} seconds'.format(car, best_time))
            self.log('fastest v: {0}'.format(self.max_v))
        print 'final max turn speeds:', self.max_turn_speeds
        self.log('final max turn speeds: {0}'.format(self.max_turn_speeds))
        self.telemetry_log.new_race_log()
        self.ping()

    def on_error(self, data):
        print('Error: {0}'.format(data))
        self.ping()

    def on_tournament_end(self, data):
        print 'tournament_end'
        self.game_id = None
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'yourCar': self.on_your_car,
            'gameInit': self.on_game_init,
            'tournamentEnd': self.on_tournament_end,
            'spawn': self.on_spawn, }
        socket_file = s.makefile()
        line = socket_file.readline()

        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if 'gameTick' in msg:
                self.game_tick = msg['gameTick']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                logging.warn('unknown msg {0}'.format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == '__main__':
    if len(sys.argv) < 5:  # != 5:
        print('Usage: ./run host port botname botkey [track_name]')
    else:
        host, port, name, key = sys.argv[1:5]
        print('Connecting with parameters:')
        print('host={0}, port={1}, bot name={2}, key={3}'.format(
            *sys.argv[1:5]))
        try:
            track_name = sys.argv[5]
        except IndexError:
            track_name = None
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = SkwishyBot(s, name, key, track_name=track_name)
        bot.run()
